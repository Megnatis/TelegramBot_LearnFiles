<?php
include '../function/function.php';
//Define get Content from Telegram Api
$Content = file_get_contents('php://input');
//Json object decode
$Object = json_decode($Content , true);
//Get data from json
$CHAT_ID = $Object['message']['chat']['id'];
$MASSAGE_ID = $Object['message']['message_id'];
$TEXT = $Object['message']['text'];

//Array For Static Keyboard
$ARRAY_MAIN_KEYBOARD = [ ['دریافت وضعیت آب و هوا']];
$ARRAY_CITY = [['تهران','شیراز'],['اصفهان','بندرعباس']];

//Define Text For user
$TEXT_WELCOME  = "سلام خوش آمدید به ربات تست لرن فایلز و ورکشاپ اول";
$TEXT_CITY_SELECT = "شهر مد نظر خود را انتخاب نمایید:";
$TEXT_ERROR = "با توجه به درخواست شما ، لطفاً از صفحه کلید استفاده نمایید.";

//Define IF AND IF AND IF
if($TEXT == 'start' or $TEXT == '/start'){
sendMessageWithKeyboard($CHAT_ID, $TEXT_WELCOME, $ARRAY_MAIN_KEYBOARD);
}
if($TEXT == 'دریافت وضعیت آب و هوا' ){
    sendMessageWithKeyboard($CHAT_ID, $TEXT_CITY_SELECT, $ARRAY_CITY);
}
switch ($TEXT) {
    case 'تهران':
        $CITY_ID = 112931;
        $Content_weather = getCurrentWeather($CITY_ID);
        $Object_weather = json_decode($Content_weather , true);
        $TEMP = $Object_weather['main']['temp'];
        $C_TEMP = (int)$TEMP - 273;
        sendMessage($CHAT_ID, "وضعیت فعلی آب و هوای شهر ".$TEXT." در حال حاضر برابر با ".$C_TEMP ." درجه سانتی گراد می باشد. ");

        break;
     case 'شیراز':
        $CITY_ID = 115019;
        $Content_weather = getCurrentWeather($CITY_ID);
        $Object_weather = json_decode($Content_weather , true);
        $TEMP = $Object_weather['main']['temp'];
        $C_TEMP = (int)$TEMP - 273;
        sendMessage($CHAT_ID, "وضعیت فعلی آب و هوای شهر ".$TEXT." در حال حاضر برابر با ".$C_TEMP ." درجه سانتی گراد می باشد. ");

        break;
     case 'اصفهان':
        $CITY_ID = 418863;
        $Content_weather = getCurrentWeather($CITY_ID);
        $Object_weather = json_decode($Content_weather , true);
        $TEMP = $Object_weather['main']['temp'];
        $C_TEMP = (int)$TEMP - 273;
        sendMessage($CHAT_ID, "وضعیت فعلی آب و هوای شهر ".$TEXT." در حال حاضر برابر با ".$C_TEMP ." درجه سانتی گراد می باشد. ");

        break;
     case 'بندرعباس':
        $CITY_ID = 141681;
        $Content_weather = getCurrentWeather($CITY_ID);
        $Object_weather = json_decode($Content_weather , true);
        $TEMP = $Object_weather['main']['temp'];
        $C_TEMP = (int)$TEMP - 273;
        sendMessage($CHAT_ID, "وضعیت فعلی آب و هوای شهر ".$TEXT." در حال حاضر برابر با ".$C_TEMP ." درجه سانتی گراد می باشد. ");

        break;
    default:
        sendMessage($CHAT_ID, $TEXT_ERROR);
        break;
}


