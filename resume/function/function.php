<?php
function getRandomStringFromArray ($ARRAY){
    return $ARRAY[array_rand($ARRAY)];
}
function convertNumber($string) {
    $persinaDigits1= array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
    $persinaDigits2= array('٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١', '٠');
    $allPersianDigits=array_merge($persinaDigits1, $persinaDigits2);
    $replaces = array('0','1','2','3','4','5','6','7','8','9','0','1','2','3','4','5','6','7','8','9');
    return str_replace($allPersianDigits, $replaces , $string);
}
function copyImage($PATH,$IMG_NAME){
    copy($PATH, '../images/'.$IMG_NAME.'.jpeg');
}
