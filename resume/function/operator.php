<?php
error_reporting(0);
//Define API TELEGRAM
define('API_TOKEN', '339715582:AAH3voo_fefpPo4Z9XO5uU3EI2q0vW7zYWo');
define('API_REQUEST', 'https://api.telegram.org/bot'.API_TOKEN.'/');
//Define Function 
function sendMessage($CHAT_ID , $MASSAGE){
    $METHOD = "sendMessage";
    $REQUEST_TO_SERVER = API_REQUEST . $METHOD . "?"."chat_id=".$CHAT_ID."&"."text=".$MASSAGE;
    file_get_contents($REQUEST_TO_SERVER);
}
function sendMessageWithKeyboard($CHAT_ID , $MASSAGE , $Keyboard){
    $METHOD = "sendMessage";
    $Option_keyboard = array("keyboard"=>$Keyboard , "resize_keyboard"=>true,"one_time_keyboard"=>true);
    $Json_replay_keyboard = json_encode($Option_keyboard);
    $REQUEST_TO_SERVER = API_REQUEST . $METHOD . "?"."chat_id=".$CHAT_ID."&"."text=".$MASSAGE ."&". "reply_markup=" .$Json_replay_keyboard;
    file_get_contents($REQUEST_TO_SERVER);
}
function sendMessageWithInlineKeyboard($CHAT_ID , $MASSAGE , $InlineKeyboard){
    $METHOD = "sendMessage";
    $Option_keyboard = array("inline_keyboard"=>$InlineKeyboard , "resize_keyboard"=>true,"one_time_keyboard"=>true);
    $Json_replay_keyboard = json_encode($Option_keyboard);
    $REQUEST_TO_SERVER = API_REQUEST . $METHOD . "?"."chat_id=".$CHAT_ID."&"."text=".$MASSAGE ."&". "reply_markup=" .$Json_replay_keyboard;
    file_get_contents($REQUEST_TO_SERVER);
}
function answerCallbackQuery($CALL_BACK_ID , $MASSAGE){
    $METHOD = "answerCallbackQuery";
    $REQUEST_TO_SERVER = API_REQUEST . $METHOD . "?"."callback_query_id=".$CALL_BACK_ID."&"."text=".$MASSAGE;
    file_get_contents($REQUEST_TO_SERVER);
}
function sendPhoto($CHAT_ID,$PHOTO_ID){
     $METHOD = "sendPhoto";
    $REQUEST_TO_SERVER = API_REQUEST . $METHOD . "?"."chat_id=".$CHAT_ID."&"."photo=".$PHOTO_ID;
    file_get_contents($REQUEST_TO_SERVER);
}
function sendDocument($CHAT_ID,$DOC_ID){
     $METHOD = "sendDocument";
    $REQUEST_TO_SERVER = API_REQUEST . $METHOD . "?"."chat_id=".$CHAT_ID."&"."document=".$DOC_ID;
    file_get_contents($REQUEST_TO_SERVER);
}
function getFile($FILE_ID){
    $METHOD = "getFile";
    $REQUEST_TO_SERVER = API_REQUEST . $METHOD . "?"."file_id=".$FILE_ID;
    $Content =  file_get_contents($REQUEST_TO_SERVER);
    $Object_json = json_decode($Content , true);
    $FILE_PATH = $Object_json['result']['file_path'];
    return "https://api.telegram.org/file/bot".API_TOKEN."/".$FILE_PATH;
}

