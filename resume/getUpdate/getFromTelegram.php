<?php
error_reporting(0);
include '../function/operator.php';
include '../../helper/config.php';
include '../../helper/jdf.php';
include '../function/function.php';
include '../function/CVMaker.php';
//Define Information getFromTelegram -> API TELEGRAM
$Content = file_get_contents('php://input');
$Object_json = json_decode($Content , true);
//Define Var
$CHARSET = "SET NAMES utf8;";
$CHAT_ID = $Object_json['message']['chat']['id'];
$TEXT = $Object_json['message']['text'];
$USER_NAME = $Object_json['message']['chat']['username'];
$F_NAME = $Object_json['message']['chat']['first_name'];
$L_NAME = $Object_json['message']['chat']['last_name'];
$Document = $Object_json["message"]["document"]["file_id"];
$PHOTO = $Object_json["message"]["photo"][0]["file_id"];
$PHONE = "";
$DATEMES = $Object_json['message']['date'];
$DATE = jdate('Y-m-d H:i:s',$DATEMES,"","","en");
$STATUS = null;
//CALL BACK DATA
$CALL_BACK_CHAT_ID = $Object_json["callback_query"]["from"]["id"];
$CALL_BACK_DATA = $Object_json["callback_query"]["data"];
$CALL_BACK_ID = $Object_json["callback_query"]["id"];
//Define TEXT ARRAY TO SEND USERS
$ARRAY_TEXT_FOR_WELCOME = array("سلام خوش آمدید به سامانه رزومه ساز","وقت شما بخیر و از اینکه ما را به عنوان سازنده رزومه خود انتخاب کردید کمال تشکر را دارم","سامانه ما با امکان تولید رزومه برای شما، ورود شما را به دنیای حرفه ای کسب و کار تبریک میگوید.");
$ARRAY_TEXT_INVAILD_COMMENDS = array("من متوجه نشدم که میخوای چی بگی؟ میخوای بیشتر کمکت کنم؟");
$ARRAY_TEXT_GET_NAME = array("من فرشید نقی زاده هستم، برای کمک به شما در این سامانه حضور دارم ، میتونیم قدم به قدم برای ثبت مشخصات شما برای ساخت یک رزومه عالی قدم برداریم،خب در قدم اول برای ساخت یک رزومه عالی نام خود رو به من بگو:");
//STATIC KEYBOARDS
$KEYBOARD_START_MENU = [['ساخت رزومه برای من'],['مشاهده رزومه من']];
$KEYBOARD_SELECT_CATEGORY = [['مشخصات فردی'],['دوره ها و گواهینامه ها','تحصیلات'],['سوابق کاری'],['زبان های خارجی','مهارت های رایانه'],['نوع همکاری'],['بازگشت']];
$KEYBOARD_SPEC_CATEGORY = [['تاریخ تولد','نام خانوادگی','نام'],['محل تولد','نام پدر','جنسیت'],['وضعیت نظام وظیفه'],['تعداد فرزند','وضعیت تاهل'],['تلفن تماس','آدرس محل سکونت','عکس پرسنلی'],['بازگشت']];
$KEYBOARD_EDU_CATEGORY = [['مقطع تحصیلی'],['نام دانشگاه','گرایش تحصیلی','رشته تحصیلی'],['معدل','شهر محل تحصیل','سال اتمام'],['مشاهده سوابق تحصیلی'],['بازگشت']];
$KEYBOARD_A_CATEGORY = [['نام دوره'],['مدت دوره به ساعت','امکان ارائه گواهینامه'],['مشاهده سوابق دوره ها'],['بازگشت']];
$KEYBOARD_A_YES_OR_NO = [['خیر','بله'],['حذف'],['بازگشت']];
$KEYBOARD_WORK_CATEGORY = [['نام سازمان یا شرکت محل خدمت'],['سمت','مدت همکاری'],['امکان ارائه مدرک','علت قطع رابطه'],['نوع همکاری'],['مشاهده سوابق کاری'],['بازگشت']];
$KEYBOARD_WORK_TYPE = [['بلند مدت، پاره وقت','بلند مدت ، تمام وقت'],['کوتاه مدت، پاره وقت','کوتاه مدت ،تمام وقت'],['پروژه ای'],['بازگشت','حذف']];
$KEYBOARD_TYPE_COP = [['بلند مدت، پاره وقت','بلند مدت ، تمام وقت'],['کوتاه مدت، پاره وقت','کوتاه مدت ،تمام وقت'],['پروژه ای'],['بازگشت']];
$KEYBOARD_L_CATEGORY = [['نام زبان'],['نوع توانایی','سطح زبان'],['مشاهده مهارت های زبانی'],['بازگشت']];
$KEYBOARD_C_CATEGORY = [['نام مهارت رایانه ای'],['نوع توانایی','سطح مهارت'],['مشاهده مهارت ها'],['بازگشت']];
$KEYBOARD_LVL = [['کاربردی','مقدماتی'],['متخصص','پیشرفته'],['بازگشت','حذف']];
$KEYBOARD_GENDER = [['خانم','آقا'],['بازگشت']];
$KEYBOARD_SINGEL = [['متاهل','مجرد'],['بازگشت']];
$KEYBOARD_SOLD = [['منتظر خدمت','اتمام خدمت'],['درحال انجام خدمت','خانم هستم!'],['بازگشت']];
$KEYBOARD_PIC = [['تصویری ندارم'],['بازگشت']];
$KEYBOARD_EDU_LVL = [['کاردانی','دیپلم','زیردیپلم'],['دکترا','کارشناسی ارشد','کارشناسی'],['حذف مقطع'],['بازگشت']];
$KEYBOARD_BACK_DELETE = [['حذف'],['بازگشت']];
$KEYBOARD_BACK = [['بازگشت']];
//STATIC TEXT
$TEXT_INVALID = "متاسفانه این مورد شناسایی نشد! یکی از گزینه های صفحه کلید رو انتخاب کنید";
//CHAR UT8 CHARSET
$DB_CONNECTING -> query($CHARSET);
//GET TEXT 
if($CHAT_ID != '' && isset($CHAT_ID) && $CHAT_ID != null){
    $QUERY = "SELECT `KEY` FROM `TBL_KEY` WHERE `CHAT_ID`=$CHAT_ID";
    $RES = $DB_CONNECTING -> query($QUERY);
    $GET_KEY = $RES -> fetch_array(MYSQLI_ASSOC);
    $KEY = $GET_KEY['KEY'];
    if($TEXT == 'start' or $TEXT =='/start'){
        $QUERY ="INSERT INTO `TBL_USER`(`CHAT_ID`, `USERNAME`, `F_NAME`, `L_NAME`, `PHONE`, `DATE`, `STATUS`)"
                . " VALUES ($CHAT_ID,'$USER_NAME','$F_NAME','$L_NAME','$PHONE','$DATE',0)";
        $DB_CONNECTING -> query($QUERY);
        $QUERY ="DELETE FROM `TBL_KEY` WHERE `CHAT_ID`=$CHAT_ID";
        $DB_CONNECTING -> query($QUERY);
        $QUERY = "INSERT INTO `TBL_KEY`(`CHAT_ID`, `KEY`, `DATE`) VALUES ($CHAT_ID,0,'$DATE')";
        $DB_CONNECTING -> query($QUERY);
        sendMessageWithKeyboard($CHAT_ID, getRandomStringFromArray($ARRAY_TEXT_FOR_WELCOME),$KEYBOARD_START_MENU);
    }
    if($TEXT == 'reset' or $TEXT =='/reset'){
        $QUERY ="DELETE FROM `TBL_KEY` WHERE `CHAT_ID`=$CHAT_ID";
        $DB_CONNECTING -> query($QUERY);
        $QUERY = "INSERT INTO `TBL_KEY`(`CHAT_ID`, `KEY`, `DATE`) VALUES ($CHAT_ID,0,'$DATE')";
        $DB_CONNECTING -> query($QUERY);
        sendMessageWithKeyboard($CHAT_ID, getRandomStringFromArray($ARRAY_TEXT_FOR_WELCOME),$KEYBOARD_START_MENU);
    }
    
    switch ($KEY) {
    case 0:
        if($TEXT == 'ساخت رزومه برای من'){
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=100,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
            $DB_CONNECTING -> query("INSERT INTO `TBL_CV`(`S_CHAT_ID`) VALUES ($CHAT_ID)");
            sendMessageWithKeyboard($CHAT_ID, 'برای ساخت یک رزومه خوب باید سعی کنید تمام مدارک و مستندات موجود خودتون رو کنار خودتون جمع آوری کنید و بعد بر اساس نیاز اون ها رو وارد سیستم کنید توجه کنید که در اینجا شما باید برای ساخت رزومه دقیق و زیبا حتماً دقیقا موارد خواسته شده را بدون هیچگونه کاستی یا اضافه گویی در سیستم وارد نمایید، حال برای شروع یکی از گزینه ها را انتخاب نمایید:', $KEYBOARD_SELECT_CATEGORY);
        }
        if($TEXT == 'مشاهده رزومه من'){//EXPORT CV IN PDF
           //$RES = $DB_CONNECTING -> query("SELECT * FROM `TBL_CV` WHERE `S_CHAT_ID`=$CHAT_ID");
           //while($row = $RES->fetch_object()){$rows[] = $row;}
           //$S_PIC = $rows[0]->S_PIC;
           //$S_PIC = explode("-", $S_PIC);
           //copyImage(getFile($S_PIC[1]),$S_PIC[1]);
           //sendPhoto($CHAT_ID, "AgADBAADk6kxG18fwFMkores08VVopvi-RkABLEOxHVR-bOg7ZcCAAEC");
//           $PATH =  getFile("AgADBAADk6kxG18fwFMkores08VVopvi-RkABLEOxHVR-bOg7ZcCAAEC");
//           sendMessage($CHAT_ID, $PATH);
//           copyImage( $PATH, $CHAT_ID);
            $KEYBOARD_PHONE_GEO = [[['text'=>'درخواست تلفن','request_contact'=>true],['text'=>'درخواست موقعیت','request_location'=>true]]];
                sendMessageWithKeyboard($CHAT_ID, "درخواست تلفن و موقعیت", $KEYBOARD_PHONE_GEO);
           sendMessage($CHAT_ID, "درخواست تایید شد");
            
            
            
            
            
            
            
            
            
            
        }
        break;
    case -1:
            $DB_CONNECTING -> query("UPDATE `TBL_USER` SET `F_NAME`='$TEXT' WHERE `CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("INSERT INTO `TBL_NAME`(`NAME`) VALUES ('$TEXT')");//ADD name be TBL_NAME
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=110,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "باموفقیت نام ".$TEXT." ثبت گردید.", $KEYBOARD_SPEC_CATEGORY);
        break;
    case -2 :
            $DB_CONNECTING -> query("UPDATE `TBL_USER` SET `L_NAME`='$TEXT' WHERE `CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("INSERT INTO `TBL_FAMILIY`(`FAMILIY`) VALUES ('$TEXT')");//ADD name be TBL_FAMILIY
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=110,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "نام خانوادگی  ".$TEXT." ثبت گردید.", $KEYBOARD_SPEC_CATEGORY);
        break;
    case 1://PHONE
        if(preg_match("/[\d]{11}/", $TEXT) OR preg_match("/[\u0660-\u0669]{11}/", $TEXT) OR preg_match("/[\u06F0-\u06F9]{11}/", $TEXT)
               OR preg_match("/[۰۱۲۳۴۵۶۷۸۹]{11}/", $TEXT) OR preg_match("/[\u0600-\u06FF]{11}/", $TEXT)){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `S_PHONE`='".convertNumber($TEXT)."' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=110,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "شماره تماس ".convertNumber($TEXT)." ثبت گردید.", $KEYBOARD_SPEC_CATEGORY);
               }//regex number format phone!
               else {
                   sendMessage($CHAT_ID, "شماره تماس ".$TEXT."قابل ثبت نیست، شماره خود را بررسی نمایید و مجدداً ارسال کنید");
               }
        break;
    case 2://BIRTH DAY
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `S_BR_DATE`='$TEXT' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=110,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "تاریخ تولد ".$TEXT." ثبت گردید.", $KEYBOARD_SPEC_CATEGORY);
        break;
    case 3://GENDER
        $ARRAY_GENDER = ['خانم','آقا'];
        if(in_array($TEXT, $ARRAY_GENDER)){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `S_GENDER`='$TEXT' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=110,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "تاریخ تولد ".$TEXT." ثبت گردید.", $KEYBOARD_SPEC_CATEGORY);
        }else{
            sendMessage($CHAT_ID, "برای ثبت جنیست از طریق صفحه کلید اقدام نمایید");
        }
        break;
    case 4://FATHER NAME
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `S_FATHER_NAME`='$TEXT' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=110,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "نام پدر ".$TEXT." ثبت گردید.", $KEYBOARD_SPEC_CATEGORY);
        break;
    case 5://BIRTH PLACE
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `S_BR_PLACE`='$TEXT' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=110,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "محل تولد با نام ".$TEXT." ثبت گردید.", $KEYBOARD_SPEC_CATEGORY);
        break;
    case 6://SINGEL
        $ARRAY_SINGEL = ['متاهل','مجرد'];
        if(in_array($TEXT, $ARRAY_SINGEL)){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `S_ST_SINGLE`='$TEXT' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=110,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "وضعیت تاهل ".$TEXT." ثبت گردید.", $KEYBOARD_SPEC_CATEGORY);
        }else{
            sendMessage($CHAT_ID, "برای ثبت وضعیت از طریق صفحه کلید اقدام نمایید");
        }
        break;
    case 7://CHILD NUMBER
        $NUM = (int) convertNumber($TEXT);
        if($NUM >= 0){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `S_NUM_CHILD`=$NUM WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=110,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "تعداد فرزند  ".$NUM." ثبت گردید.", $KEYBOARD_SPEC_CATEGORY);
        }else{
             sendMessage($CHAT_ID, "فقط تعداد فرزندان وارد شود، درصورت مجرد بودن عدد 0 وارد گردد.");
        }
        break;
    case 8://SOLD
        if(in_array($TEXT, ['منتظر خدمت','اتمام خدمت','درحال انجام خدمت','خانم هستم!'])){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `S_ST_SOLD`='$TEXT' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=110,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "وضعیت خدمت  ".$TEXT." ثبت گردید.", $KEYBOARD_SPEC_CATEGORY);
        } else {
            sendMessage($CHAT_ID, "وضعیت خدمت را از طریق صفحه کلید مشخص نمایید");
    }
        break;
    case 9://ADRESS
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `S_ADDRES`='$TEXT' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=110,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "آدرس:"."%0A".$TEXT."%0A"." ثبت گردید.", $KEYBOARD_SPEC_CATEGORY);
        break;
    case 10:
        if($TEXT == 'تصویری ندارم'){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `S_PIC`='IM_NOPIC0000' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=110,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "هیچ تصویری برای شما ثبت نگردید!", $KEYBOARD_SPEC_CATEGORY);
        }
        if($Document != null OR $PHOTO != null){
        $PIC = ($PHOTO == null) ? "DC-".$Document : "PC-".$PHOTO ;
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `S_PIC`='$PIC' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=110,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "تصویر شما ثبت گردید!", $KEYBOARD_SPEC_CATEGORY);
        } else {
        sendMessage($CHAT_ID, 'تصویر شما قابل شناسایی نیست لطفاً آن را تغیر دهید');    
        }
        break;
    case 13://LVL
    if(in_array($TEXT, ['کاردانی','دیپلم','زیردیپلم','دکترا','کارشناسی ارشد','کارشناسی'])){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `E_LVL`=CONCAT( TBL_CV.E_LVL , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=120,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "مقطع تحصیلی ".$TEXT." ثبت گردید", $KEYBOARD_EDU_CATEGORY);
    }else if($TEXT == 'حذف مقطع'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `E_LVL`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "مقاطع تحصیلی حذف گردید، چنانچه نیاز به افزودن دو یا چند مقطع همزمان دارید پس از ثبت مقطع مجدداً به این قسمت برگردید");
    }
    else{
        sendMessage($CHAT_ID, "برای انتخاب مقطع تحصیلی از صفحه کلید استفاده نمایید.");
    }
        break;
    case 14://FEILD
        if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `E_FIELD`=CONCAT( TBL_CV.E_FIELD , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=120,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "رشته تحصیلی  ".$TEXT." ثبت گردید", $KEYBOARD_EDU_CATEGORY);
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `E_FIELD`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        break;
    case 15:
         if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `E_SUB_FIELD`=CONCAT( TBL_CV.E_SUB_FIELD , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=120,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "گرایش تحصیلی  ".$TEXT." ثبت گردید", $KEYBOARD_EDU_CATEGORY);
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `E_SUB_FIELD`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        break;
    case 16:
        if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `E_TIME`=CONCAT( TBL_CV.E_TIME , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=120,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "سال پایان تحصیل  ".$TEXT." ثبت گردید", $KEYBOARD_EDU_CATEGORY);
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `E_TIME`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        break;
    case 17:
        if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `E_NAME_UNI`=CONCAT( TBL_CV.E_NAME_UNI , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=120,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "نام  ".$TEXT." ثبت گردید", $KEYBOARD_EDU_CATEGORY);
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `E_NAME_UNI`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        break;
    case 18:
        if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `E_STATE_COUNTRY`=CONCAT( TBL_CV.E_STATE_COUNTRY , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=120,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "شهر  ".$TEXT." ثبت گردید", $KEYBOARD_EDU_CATEGORY);
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `E_STATE_COUNTRY`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        break;
    case 19:
        if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `E_AVRG`=CONCAT( TBL_CV.E_AVRG , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=120,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "معدل  ".$TEXT." ثبت گردید", $KEYBOARD_EDU_CATEGORY);
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `E_AVRG`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        break;
    case 20:
        if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `A_NAME`=CONCAT( TBL_CV.A_NAME , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=130,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "دوره با نام ".$TEXT." ثبت گردید", $KEYBOARD_A_CATEGORY);
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `A_NAME`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        break;
   case 21:
       if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `A_TIME`=CONCAT( TBL_CV.A_TIME , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=130,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "طول دوره ".$TEXT." ساعت ثبت گردید", $KEYBOARD_A_CATEGORY);
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `A_TIME`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        break;
   case 22:
       if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `A_CERTI`=CONCAT( TBL_CV.A_CERTI , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=130,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "معدل  ".$TEXT." ثبت گردید", $KEYBOARD_A_CATEGORY);
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `A_CERTI`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        break;
    case 23:
        if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `W_NAME_COMPANY`=CONCAT( TBL_CV.W_NAME_COMPANY , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=140,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "نام ".$TEXT." ثبت گردید", $KEYBOARD_WORK_CATEGORY);
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `W_NAME_COMPANY`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        break;
    case 24:
        if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `W_TIME`=CONCAT( TBL_CV.W_TIME , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=140,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "مدت همکاری ".$TEXT."ماه ثبت گردید", $KEYBOARD_WORK_CATEGORY);
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `W_TIME`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        break;
    case 25:
        if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `W_SEMAT`=CONCAT( TBL_CV.W_SEMAT , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=140,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "سمت شما ".$TEXT." ثبت گردید", $KEYBOARD_WORK_CATEGORY);
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `W_SEMAT`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        break;
    case 26:
        if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `W_DESCONECT`=CONCAT( TBL_CV.W_DESCONECT , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=140,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "علت قطع رابطه ".$TEXT." ثبت گردید", $KEYBOARD_WORK_CATEGORY);
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `W_DESCONECT`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        break;
    case 27:
         if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
             if(in_array($TEXT, ['خیر','بله'])){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `W_LICENCE`=CONCAT( TBL_CV.W_LICENCE , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=140,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "امکان ارائه مدرک  ".$TEXT." ثبت گردید", $KEYBOARD_WORK_CATEGORY);
             }
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `W_LICENCE`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        else{
            sendMessage($CHAT_ID, "لطفاً از صفحه کلید استفاده نمایید");
        }
        break;
    case 28:
        if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
            if(in_array($TEXT, ['بلند مدت، پاره وقت','بلند مدت ، تمام وقت','کوتاه مدت، پاره وقت','کوتاه مدت ،تمام وقت','پروژه ای'])){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `W_TYPE`=CONCAT( TBL_CV.W_TYPE , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=140,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "علت قطع رابطه ".$TEXT." ثبت گردید", $KEYBOARD_WORK_CATEGORY);
            }
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `W_TYPE`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        else{
            sendMessage($CHAT_ID, "لطفاً از صفحه کلید استفاده نمایید");
        }
        break;
    case 29:
        if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `L_NAME`=CONCAT( TBL_CV.L_NAME , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=150,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "نام زبان  ".$TEXT." ثبت گردید", $KEYBOARD_L_CATEGORY);
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `L_NAME`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        break;
        case 30:
        if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `L_TYPE`=CONCAT( TBL_CV.L_TYPE , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=150,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "نوع توانایی ".$TEXT." ثبت گردید", $KEYBOARD_L_CATEGORY);
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `L_TYPE`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        break;
        case 31:
        if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
            if(in_array($TEXT, ['کاربردی','مقدماتی','متخصص','پیشرفته'])){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `L_LVL`=CONCAT( TBL_CV.L_LVL , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=150,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "نوع توانایی ".$TEXT." ثبت گردید", $KEYBOARD_L_CATEGORY);
            }
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `L_LVL`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        break;
        
        case 32:
        if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `C_NAME`=CONCAT( TBL_CV.C_NAME , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=160,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "نام زبان  ".$TEXT." ثبت گردید", $KEYBOARD_C_CATEGORY);
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `C_NAME`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        break;
        case 33:
        if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `C_TYPE`=CONCAT( TBL_CV.C_TYPE , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=160,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "نوع توانایی ".$TEXT." ثبت گردید", $KEYBOARD_C_CATEGORY);
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `C_TYPE`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        break;
        case 34:
        if($TEXT != 'حذف' && $TEXT != 'بازگشت'){
            if(in_array($TEXT, ['کاربردی','مقدماتی','متخصص','پیشرفته'])){
            $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `C_LVL`=CONCAT( TBL_CV.C_LVL , '$TEXT;' ) WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=160,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");//UPDATE KEY
            sendMessageWithKeyboard($CHAT_ID, "نوع توانایی ".$TEXT." ثبت گردید", $KEYBOARD_C_CATEGORY);
            }
        }else if($TEXT == 'حذف'){
        $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `C_LVL`='' WHERE `S_CHAT_ID`=$CHAT_ID");//ADD name be TBL_USER
        sendMessage($CHAT_ID, "تمام اطلاعات این قسمت پاک شد، حالا می توانید برای ثبت مجدد اقدام نمایید، چنانچه قبلا ورودی داشتید اول آن را وارد کنید و بعد از ثبت مجدداً به این قسمت وارد شده و ثبت را انجام دهید");
        }
        break;
        
    case 100:
        $ARRAY_SELECT_CATEGORY = ['مشخصات فردی','دوره ها و گواهینامه ها','تحصیلات','سوابق کاری','زبان های خارجی','مهارت های رایانه','نوع همکاری'];
        if(in_array($TEXT,$ARRAY_SELECT_CATEGORY )){
            switch ($TEXT) {
                case 'مشخصات فردی':
                     $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=110,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                     sendMessageWithKeyboard($CHAT_ID, "مشخصات فردی خود را با انتخاب گزینه های موجود کامل نمایید:", $KEYBOARD_SPEC_CATEGORY);
                    break;
                  case 'تحصیلات':
                      $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=120,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                      sendMessageWithKeyboard($CHAT_ID, "سوابق تحصیلی خود را کامل نماید، در صورت داشتن بیش از یک سابقه می توانید پس از ثبت کامل یک دوره مجدداً وارد این قسمت شده و دوره بعد را نیز وارد نمایید", $KEYBOARD_EDU_CATEGORY);
                    break;
                case 'دوره ها و گواهینامه ها':
                       $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=130,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                      sendMessageWithKeyboard($CHAT_ID, "سوابق گواهینامه های اخذ شده و دوره های گذرانده خود را وارد نمایید", $KEYBOARD_A_CATEGORY);
                    break;
                  case 'سوابق کاری':
                       $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=140,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                       sendMessageWithKeyboard($CHAT_ID, "سوابق کاری خود را به دقت وارد نماید", $KEYBOARD_WORK_CATEGORY);
                    break;
                  case 'زبان های خارجی':
                       $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=150,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                      sendMessageWithKeyboard($CHAT_ID, "مهارت های خود را در زبان های خارجی وارد نمایید", $KEYBOARD_L_CATEGORY);
                    break;
                  case 'مهارت های رایانه':
                       $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=160,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                      sendMessageWithKeyboard($CHAT_ID, "مهارت های خود را در زمینه های رایانه ای وارد نمایید", $KEYBOARD_C_CATEGORY);
                    break;
                case 'نوع همکاری':
                    $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=170,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                      sendMessageWithKeyboard($CHAT_ID, "نوع همکاری خود را با سازمان ها یا شرکت ها برای ثبت در رزومه مشخص نمایید", $KEYBOARD_TYPE_COP);
                break;

            }
        }else if($TEXT == 'بازگشت'){
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=0,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
            sendMessageWithKeyboard($CHAT_ID, "برای مشاهده رزومه خود می توانید از دکمه مشاهده رزومه استفاده نمایید", $KEYBOARD_START_MENU);
        }
        else{
            sendMessage($CHAT_ID,$TEXT_INVALID);
        }
        break;
                case 110:
                    $ARRAY_SELECT_SPEC = ['تاریخ تولد','نام خانوادگی','نام','محل تولد','نام پدر','جنسیت','وضعیت نظام وظیفه','تعداد فرزند','وضعیت تاهل','تلفن تماس','آدرس محل سکونت','عکس پرسنلی'];
                    if(in_array($TEXT, $ARRAY_SELECT_SPEC)){
                        switch ($TEXT) {
                            case 'نام'://-1
                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=-1,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "تنها نام خود را وارد نمایید، بطور مثال : محمد، علی ، امیرحسین",$KEYBOARD_BACK);
                                break;
                            case 'نام خانوادگی'://-2
                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=-2,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "نام خانوادگی خود را وارد نمایید:",$KEYBOARD_BACK);
                                break;
                            case 'تلفن تماس'://1
                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=1,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "شماره تماس خود را مانند نمونه وارد نمایید:09123456789",$KEYBOARD_BACK);
                                break;
                            case 'تاریخ تولد'://2
                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=2,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "تاریخ تولد خود را مانند نمونه وارد نمایید: 15/مهر/1370",$KEYBOARD_BACK);
                                break;
                            case 'جنسیت'://3
                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=3,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "از طریق صفحه کلید پیش رو جنیست خود را مشخص نمایید:",$KEYBOARD_GENDER);
                                break;
                            case 'نام پدر'://4
                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=4,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "نام پدر خود را وارد نمایید:",$KEYBOARD_BACK);
                                break;
                            case 'محل تولد'://5
                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=5,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "محل تولد خود را وارد نمایید،مانند : تهران، مشهد و غیره...",$KEYBOARD_BACK);
                                break;
                            case 'وضعیت تاهل'://6
                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=6,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "وضعیت تاهل خود را مشخص نمایید:",$KEYBOARD_SINGEL);
                                break;
                            case 'تعداد فرزند'://7
                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=7,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "تعداد فرزندان خود را مشخص نمایید، مانند: 0،1 یا 5",$KEYBOARD_BACK);
                                break;
                            case 'وضعیت نظام وظیفه'://8
                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=8,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "وضعیت خدمت خود را مشخص نمایید:",$KEYBOARD_SOLD);
                                break;
                            case 'آدرس محل سکونت'://9
                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=9,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "آدرس محل سکونت خود را دقیق مشخص نمایید، با ذکر شهرستان و استان",$KEYBOARD_BACK);
                                break;
                            case 'عکس پرسنلی'://10
                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=10,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "تصویر خود را برای ثبت در رزومه ارسال کنید،توجه کنید عکس شما عکس پرسنلی باشد و از عکس های متفرقه استفاده نکنید!",$KEYBOARD_PIC);
                                break;  
                        }
                       
                    }else if($TEXT == 'بازگشت'){
                        $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=100,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "دسته بندی مورد نظر را انتخاب نماید",$KEYBOARD_SELECT_CATEGORY);
                    }
                    
                    break;
                            case 120:
                                switch ($TEXT) {
   //[['مقطع تحصیلی'],['نام دانشگاه','گرایش تحصیلی','رشته تحصیلی'],['معدل','شهر محل تحصیل','سال اتمام'],['مشاهده سوابق تحصیلی'],['بازگشت']];
                                    case 'مقطع تحصیلی'://13
                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=13,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "مقطع تحصیلی خود را مشخص نمایید:",$KEYBOARD_EDU_LVL);
                                        break;
                                    case 'رشته تحصیلی'://14
                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=14,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "رشته تحصیلی خود را بدون ذکر گرایش وارد نمایید",$KEYBOARD_BACK_DELETE);
                                        break;
                                        case 'گرایش تحصیلی'://15
                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=15,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "تنها گرایش تحصیلی خود را وارد نمایید، در صورت عدم وجود از - استفاده کنید",$KEYBOARD_BACK_DELETE);
                                        break;
                                     case 'سال اتمام'://16
                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=16,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "سال اتمام دوره تحصیلی خود را مانند نمونه وارد نمایید: 89 یا 90",$KEYBOARD_BACK_DELETE);
                                        break;
                                     case 'نام دانشگاه'://17
                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=17,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "نام دانشگاه خود را وارد نمایید: بطور مثال : دانشگاه هرمزگان یا دانشگاه آزاد بیرجند",$KEYBOARD_BACK_DELETE);
                                        break;
                                     case 'شهر محل تحصیل'://18
                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=18,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "شهر محل تحصیل را وارد نمایید:",$KEYBOARD_BACK_DELETE);
                                        break;
                                    case 'معدل'://19
                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=19,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "معدل خود را وارد نمایید: برای مثال 15.26 یا 17",$KEYBOARD_BACK_DELETE);
                                        break;
                                    case 'مشاهده سوابق تحصیلی'://
                                $RES = $DB_CONNECTING -> query("SELECT `E_LVL`, `E_FIELD`, `E_SUB_FIELD`, `E_TIME`, `E_NAME_UNI`, `E_STATE_COUNTRY`, `E_AVRG` FROM `TBL_CV` WHERE `S_CHAT_ID`=$CHAT_ID");
                                    while($row = $RES->fetch_object()){$rows[] = $row;}
                                    $E_LVL = $rows[0]->E_LVL;
                                    $E_FIELD = $rows[0]->E_FIELD;
                                    $E_SUB_FIELD = $rows[0]->E_SUB_FIELD;
                                    $E_TIME= $rows[0]->E_TIME;
                                    $E_NAME_UNI = $rows[0]->E_NAME_UNI;
                                    $E_STATE_COUNTRY = $rows[0]->E_STATE_COUNTRY;
                                    $E_AVRG = $rows[0]->E_AVRG;
                                    //
                                    $E_LVL = explode(';', $E_LVL);
                                    $E_FIELD = explode(';', $E_FIELD);
                                    $E_SUB_FIELD = explode(';', $E_SUB_FIELD);
                                    $E_TIME = explode(';', $E_TIME);
                                    $E_NAME_UNI = explode(';', $E_NAME_UNI);
                                    $E_STATE_COUNTRY = explode(';', $E_STATE_COUNTRY);
                                    $E_AVRG = explode(';', $E_AVRG);
                                    for ($index = 0; $index < 4; $index++) {
                                        if($E_LVL[$index] != '' OR $E_FIELD[$index] != '' OR$E_SUB_FIELD[$index] != '' OR$E_TIME[$index] != '' 
                                                OR$E_NAME_UNI[$index] != '' OR$E_STATE_COUNTRY[$index] != '' OR$E_AVRG[$index] != '' ){
                                        sendMessage($CHAT_ID, 'مقطع تحصیلی : '.$E_LVL[$index]
                                                . "%0A".'رشته تحصیلی : '.$E_FIELD[$index]
                                                . "%0A".'گرایش : '.$E_SUB_FIELD[$index]
                                                . "%0A".'تاریخ پایان تحصیل : '.$E_TIME[$index]
                                                . "%0A".'نام دانشگاه : '.$E_NAME_UNI[$index]
                                                . "%0A".'نام شهر محل تحصیل : '.$E_STATE_COUNTRY[$index]
                                                . "%0A".'معدل : '.$E_AVRG[$index]
                                                . "%0A".'اطلاعات ثبت شده شما در این دوره می باشد'
                                        );
                                        
                                        }
                                    }

                                        break;
                                    
                                  
                                }
                                break;
                        case 130:
                            //['نام دوره'],['مدت دوره به ساعت','امکان ارائه گواهینامه'],['مشاهده سوابق دوره ها']
                            switch ($TEXT) {
                                case 'نام دوره'://20
                                    $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=20,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "نام دوره یا گواهینامه خود را وارد نمایید:",$KEYBOARD_BACK_DELETE);
                                    break;
                                case 'مدت دوره به ساعت'://21
                                      $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=21,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "مدت حضور یا طول دوره را به ساعت وارد نمایید",$KEYBOARD_BACK_DELETE);
                                    break;
                                case 'امکان ارائه گواهینامه'://22
                                      $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=22,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "آیا امکان ارائه گواهینامه را دارید؟",$KEYBOARD_A_YES_OR_NO);
                                    break;
                                case 'مشاهده سوابق دوره ها':
                                    $RES = $DB_CONNECTING ->query("SELECT `A_NAME`, `A_TIME`, `A_CERTI` FROM `TBL_CV` WHERE `S_CHAT_ID`=$CHAT_ID");
                                     while($row = $RES->fetch_object()){$rows[] = $row;}
                                    $A_NAME = $rows[0]->A_NAME;
                                    $A_TIME = $rows[0]->A_TIME;
                                    $A_CERTI = $rows[0]->A_CERTI;
                                    
                                    $A_NAME = explode(';', $A_NAME);
                                    $A_TIME = explode(';', $A_TIME);
                                    $A_CERTI = explode(';', $A_CERTI);
                                    
                                     for ($index = 0; $index <= count($A_NAME); $index++) {
                                     if($A_NAME[$index] != '' && $A_TIME[$index] != '' && $A_CERTI[$index] != ''){
                                         sendMessage($CHAT_ID, "دوره های ثبت شده:"
                                                 ."%0A"."نام دوره: ".$A_NAME[$index]
                                                 ."%0A"."مدت دوره: ".$A_TIME[$index]
                                                 ."%0A"."امکان ارائه گواهینامه : ".$A_CERTI[$index]
                                                 ."%0A"."برای شما ثبت گردیده است.");
                                     }
                                     }
                                    break;

                            }
                        break;
                        
                                case 140:
                                    switch ($TEXT) {
   //['نام سازمان یا شرکت محل خدمت'],['سمت','مدت همکاری'],['امکان ارائه مدرک','علت قطع رابطه'],['نوع همکاری'],['مشاهده سوابق کاری'],['بازگشت']
                                        case 'نام سازمان یا شرکت محل خدمت':
                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=23,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "نام سازمان یا شرکت را وارد نمایید",$KEYBOARD_BACK_DELETE);
                                            break;
                                        case 'مدت همکاری':
                                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=24,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "مدت همکاری خود را به مال وارد نمایید",$KEYBOARD_BACK_DELETE);
                                            break;
                                        case 'سمت':
                                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=25,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "سمت خود را مشخص نمایید ، برای مثال مدیر فروش، جوشکار",$KEYBOARD_BACK_DELETE);
                                            break;
                                        case 'علت قطع رابطه':
                                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=26,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "دلیل قطع رابطه را خلاصه بیان نمایید، بطور مثال: عدم کفایت حقوق یا اتمام کار شرکت",$KEYBOARD_BACK_DELETE);
                                            break;
                                        case 'امکان ارائه مدرک':
                                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=27,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "آیا امکان ارائه گواهینامه دارید؟",$KEYBOARD_A_YES_OR_NO);
                                            break;
                                        
                                        case 'نوع همکاری':
                                                $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=28,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                sendMessageWithKeyboard($CHAT_ID, "نوع همکاری خود را با آن شرکت یا سازمان مشخص نمایید",$KEYBOARD_WORK_TYPE);
                                            break;
                                        case 'مشاهده سوابق کاری':
                                            $RES = $DB_CONNECTING ->query("SELECT `W_NAME_COMPANY`, `W_TIME`, `W_SEMAT`, `W_DESCONECT`, `W_LICENCE`, `W_TYPE` FROM `TBL_CV` WHERE `S_CHAT_ID`=$CHAT_ID");
                                     while($row = $RES->fetch_object()){$rows[] = $row;}
                                    $W_NAME_COMPANY = $rows[0]->W_NAME_COMPANY;
                                    $W_TIME = $rows[0]->W_TIME;
                                    $W_SEMAT = $rows[0]->W_SEMAT;
                                    $W_DESCONECT = $rows[0]->W_DESCONECT;
                                    $W_LICENCE = $rows[0]->W_LICENCE;
                                    $W_TYPE = $rows[0]->W_TYPE;
                                    
                                    $W_NAME_COMPANY = explode(';', $W_NAME_COMPANY);
                                    $W_TIME = explode(';', $W_TIME);
                                    $W_SEMAT = explode(';', $W_SEMAT);
                                    $W_DESCONECT = explode(';', $W_DESCONECT);
                                    $W_LICENCE = explode(';', $W_LICENCE);
                                    $W_TYPE = explode(';', $W_TYPE);
                                    
                                     for ($index = 0; $index <= count($W_NAME_COMPANY); $index++) {
                                     if($W_NAME_COMPANY[$index] != '' && $W_TIME[$index] != '' && $W_SEMAT[$index] != ''
                                             && $W_DESCONECT[$index] != '' &&$W_LICENCE[$index] != '' &&$W_TYPE[$index] != ''){
                                         sendMessage($CHAT_ID, "سابقه کاری شما:"
                                                 ."%0A"."نام شرکت یا سازمان: ".$W_NAME_COMPANY[$index]
                                                 ."%0A"."مدت همکاری : ".$W_TIME[$index]." ماه"
                                                 ."%0A"."سمت شما : ".$W_SEMAT[$index]
                                                 ."%0A"."علت قطع رابطه : ".$W_DESCONECT[$index]
                                                 ."%0A"."امکان ارائه مدرک : ".$W_LICENCE[$index]
                                                 ."%0A"."نوع همکاری شما : ".$W_TYPE[$index]
                                                 ."%0A"."برای شما ثبت گردیده است.");
                                     }
                                     }
                                            break;
                                        case 'بازگشت':
                                            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=110,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                            sendMessageWithKeyboard($CHAT_ID, 'انتخاب نمایید:', $KEYBOARD_SELECT_CATEGORY);
                                            break;

                                    }
                                    break;
                                        case 150:
                                          //[['نام زبان'],['نوع توانایی','سطح زبان'],['مشاهده مهارت های زبانی'],['بازگشت']];
                                           switch ($TEXT) {
                                               case 'نام زبان':
                                        $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=29,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                        sendMessageWithKeyboard($CHAT_ID, "نام زبان را وارد نمایید",$KEYBOARD_BACK);
                                                   break;
                                               case 'نوع توانایی':
                                        $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=30,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                        sendMessageWithKeyboard($CHAT_ID, "توانایی خود را در زبان وارد نمایید،بطور مثال : درک مطلب و مکالمه روان",$KEYBOARD_BACK);
                                                   break;
                                               case 'سطح زبان':
                                        $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=31,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                        sendMessageWithKeyboard($CHAT_ID, "سطح توانایی خود را مشخص نمایید",$KEYBOARD_LVL);
                                                   break;
                                               case 'مشاهده مهارت های زبانی':
                                    $RES = $DB_CONNECTING ->query("SELECT `L_NAME`, `L_TYPE`, `L_LVL` FROM `TBL_CV` WHERE `S_CHAT_ID`=$CHAT_ID");
                                     while($row = $RES->fetch_object()){$rows[] = $row;}
                                    $L_NAME = $rows[0]->L_NAME;
                                    $L_TYPE = $rows[0]->L_TYPE;
                                    $L_LVL = $rows[0]->L_LVL;
                                    
                                    $L_NAME = explode(';', $L_NAME);
                                    $L_TYPE = explode(';', $L_TYPE);
                                    $L_LVL = explode(';', $L_LVL);
                                    
                                     for ($index = 0; $index <= count($L_NAME); $index++) {
                                     if($L_NAME[$index] != '' && $L_TYPE[$index] != '' && $L_LVL[$index] != ''){
                                         sendMessage($CHAT_ID, "زبان های ثبت شده:"
                                                 ."%0A"."نام زبان: ".$L_NAME[$index]
                                                 ."%0A"."نوع توانایی: ".$L_TYPE[$index]
                                                 ."%0A"."سطح توانایی: ".$L_LVL[$index]
                                                 ."%0A"."برای شما ثبت گردیده است.");
                                     }
                                     }
                                                   break;
                                               case 'بازگشت':
                                                   $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=100,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                                   sendMessageWithKeyboard($CHAT_ID, "انتخاب نمایید", $KEYBOARD_SELECT_CATEGORY);
                                                   break;
                                           }
                                            break;
                                        case 160:
                                           //[['نام مهارت رایانه ای'],['نوع توانایی','سطح مهارت'],['مشاهده مهارت ها'],['بازگشت']];
                                            switch ($TEXT) {
                                               case 'نام مهارت رایانه ای':
                                        $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=32,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                        sendMessageWithKeyboard($CHAT_ID, "نام مهارت رایانه ای ،بطور مثال : مجموعه آفیس، فتوشاپ،برنامه نویسی به زبان سی",$KEYBOARD_BACK);
                                                   break;
                                               case 'نوع توانایی':
                                        $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=33,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                        sendMessageWithKeyboard($CHAT_ID, "نوع توانایی خود را مشخص نمایید،بطور مثال : توانایی تغییر عکس و چهره",$KEYBOARD_BACK);
                                                   break;
                                               case 'سطح مهارت':
                                        $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=34,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                        sendMessageWithKeyboard($CHAT_ID, "سطح توانایی خود را مشخص نمایید",$KEYBOARD_LVL);
                                                   break;
                                               case 'مشاهده مهارت ها':
                                    $RES = $DB_CONNECTING ->query("SELECT `C_NAME`, `C_TYPE`, `C_LVL`, `TYPE_COP` FROM `TBL_CV` WHERE `S_CHAT_ID`=$CHAT_ID");
                                     while($row = $RES->fetch_object()){$rows[] = $row;}
                                    $C_NAME = $rows[0]->C_NAME;
                                    $C_TYPE = $rows[0]->C_TYPE;
                                    $C_LVL = $rows[0]->C_LVL;
                                    
                                    $C_NAME = explode(';', $C_NAME);
                                    $C_TYPE = explode(';', $C_TYPE);
                                    $C_LVL = explode(';', $C_LVL);
                                    
                                     for ($index = 0; $index <= count($C_NAME); $index++) {
                                     if($C_NAME[$index] != '' && $C_TYPE[$index] != '' && $C_LVL[$index] != ''){
                                         sendMessage($CHAT_ID, "زبان های ثبت شده:"
                                                 ."%0A"."نام مهارت رایانه ای: ".$C_NAME[$index]
                                                 ."%0A"."نوع توانایی: ".$C_TYPE[$index]
                                                 ."%0A"."سطح توانایی: ".$C_LVL[$index]
                                                 ."%0A"."برای شما ثبت گردیده است.");
                                     }
                                     }
                                                   break;
                                               case 'بازگشت':
                                                   $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=100,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                                   sendMessageWithKeyboard($CHAT_ID, "انتخاب نمایید", $KEYBOARD_SELECT_CATEGORY);
                                                   break;

                                           }
                                            break;
                                               case 170:
                                                   if(in_array($TEXT, ['بلند مدت، پاره وقت','بلند مدت ، تمام وقت','کوتاه مدت، پاره وقت','کوتاه مدت ،تمام وقت','پروژه ای'])){
                                                   $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=100,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                                                   $DB_CONNECTING -> query("UPDATE `TBL_CV` SET `TYPE_COP`='$TEXT' WHERE `S_CHAT_ID`=$CHAT_ID");
                                                   sendMessageWithKeyboard($CHAT_ID, "نوع همکاری "." ".$TEXT." "."ثبت گردید ", $KEYBOARD_SELECT_CATEGORY);  
                                                   }else{
                                                       sendMessage($CHAT_ID, " برای انتخاب نوع همکاری از صفحه کلید استفاده نمایید");
                                                   }
                                                   break;
                                    
              case 'بازگشت':
                  $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=100,`DATE`='$DATE' WHERE `CHAT_ID` =$CHAT_ID");
                  sendMessageWithKeyboard($CHAT_ID, "دسته بندی مورد نظر را انتخاب نماید",$KEYBOARD_SELECT_CATEGORY);
                  break;
                default :
                    sendMessage($CHAT_ID, "چنانچه با این پیام مواجه شدید از طریق دستور /start یا/reset مشکل را برطرف خواهید کرد");
                    break;
}
    
}

if(isset($CALL_BACK_CHAT_ID) && $CALL_BACK_CHAT_ID != "" && isset($CALL_BACK_DATA) && $CALL_BACK_DATA != ""){
    $FORMAT_DATA  = explode("-", $CALL_BACK_DATA);
    switch ($FORMAT_DATA[0]) {
        case "OK_NAME":
                $NAME =  $FORMAT_DATA[1];  
            $DB_CONNECTING -> query("UPDATE `TBL_USER` SET `F_NAME`='$NAME' WHERE `CHAT_ID`=$CALL_BACK_CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("INSERT INTO `TBL_NAME`(`NAME`) VALUES ('$NAME')");//ADD name be TBL_NAME
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=2,`DATE`='$DATE' WHERE `CHAT_ID` =$CALL_BACK_CHAT_ID");//UPDATE KEY
            answerCallbackQuery($CALL_BACK_ID, "با موفقیت نام شما ثبت گردید!");
            sendMessageWithKeyboard($CALL_BACK_CHAT_ID, "نام خانوادگی خود را وارد نمایید:",$KEYBOARD_BACK);
            break;
        case "NO_NAME":
            answerCallbackQuery($CALL_BACK_ID, "نام خود را وارد کنید:");
            break;
        case "OK_FAMILIY":
            $FAMILIY = $FORMAT_DATA[1]; 
            $DB_CONNECTING -> query("UPDATE `TBL_USER` SET `L_NAME`='$FAMILIY' WHERE `CHAT_ID`=$CALL_BACK_CHAT_ID");//ADD name be TBL_USER
            $DB_CONNECTING -> query("INSERT INTO `TBL_FAMILIY`(`FAMILIY`) VALUES ('$FAMILIY')");//ADD name be TBL_FAMILIY
            $DB_CONNECTING -> query("UPDATE `TBL_KEY` SET `KEY`=3,`DATE`='$DATE' WHERE `CHAT_ID` =$CALL_BACK_CHAT_ID");//UPDATE KEY
            answerCallbackQuery($CALL_BACK_ID, "با موفقیت نام شما ثبت گردید!");
            sendMessageWithKeyboard($CALL_BACK_CHAT_ID, "شماره تماس خود را مانند نمونه وارد نمایید:09123456789",$KEYBOARD_BACK);
            
        break;
        case "NO_FAMILIY":
            answerCallbackQuery($CALL_BACK_ID, "نام خانوادگی خود را وارد نمایید:");
            break;
    }
}


//END OF CODING
$DB_CONNECTING -> close();
exit();