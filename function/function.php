<?php
//Define API TELEGRAM
define('API_TOKEN', '461097005:AAFqQeYKBsKkgo3Do1IhfNNmFk_yxpNWJHY');
define('API_REQUEST', 'https://api.telegram.org/bot'.API_TOKEN.'/');
//Define API WEATHER
define('API_KEY', '2bf68ae4ed0478bd9adfcd2ef41616a0');
define('API_URL', 'http://api.openweathermap.org/data/2.5/weather?APPID='.API_KEY);

function sendMessage($CHAT_ID , $MASSAGE){
    $METHOD = "sendMessage";
    $REQUEST_TO_SERVER = API_REQUEST . $METHOD . "?"."chat_id=".$CHAT_ID."&"."text=".$MASSAGE;
    return file_get_contents($REQUEST_TO_SERVER);
}
function sendMessageWithReplayToMassage($CHAT_ID , $MASSAGE , $MASSAGE_ID){
    $METHOD = "sendMessage";
    $REQUEST_TO_SERVER = API_REQUEST . $METHOD . "?"."chat_id=".$CHAT_ID."&"."text=".$MASSAGE."&"."reply_to_message_id=".$MASSAGE_ID;
    file_get_contents($REQUEST_TO_SERVER);
}
function sendMessageWithKeyboard($CHAT_ID , $MASSAGE , $Keyboard){
    $METHOD = "sendMessage";
    $Option_keyboard = array("keyboard"=>$Keyboard , "resize_keyboard"=>true,"one_time_keyboard"=>true);
    $Json_replay_keyboard = json_encode($Option_keyboard);
    $REQUEST_TO_SERVER = API_REQUEST . $METHOD . "?"."chat_id=".$CHAT_ID."&"."text=".$MASSAGE ."&". "reply_markup=" .$Json_replay_keyboard;
    file_get_contents($REQUEST_TO_SERVER);
}
function getCurrentWeather($CITY_ID){
    $REQUEST_TO_WEATHER_SERVER = API_URL . "&id=" . $CITY_ID;
    return file_get_contents($REQUEST_TO_WEATHER_SERVER);
}